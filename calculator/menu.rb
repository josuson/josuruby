require_relative 'operations'

module Calculator
  class Menu
    def initialize
      puts "Bem vindo, para prosseguir escolha uma das seguintes opções"
      puts "1. Média preconceituosa"
      puts "2. Calculo sem números"
      puts "3. Filtrar filmes"
      puts "4. Finalizar"
      puts "Eu escolho: "
    optionMenu = gets.chomp.to_i

    case optionMenu
    when 1
      puts "Insira o JSON com os nomes dos alunos e suas respectivas notas"
      grades = gets.chomp
      puts "Insira a lista com os alunos que não serão incluídos:"
      blacklist = gets.chomp
      pontuação = Operations.new
      puts "Média: #{pontuação.biased_mean(grades,blacklist)}"
    when 2
      puts "Insira o número a ser computado:"
      numbers = gets.chomp.to_s
      numero = Operations.new
      puts numero.no_intergers(numbers)
    when 3
      puts "Insira o gênero do filme:"
      genres = gets.chomp
      puts "Insira  o ano do filme"
      year = gets.chomp
      filme = Operations.new
      puts filme.filter_films(genres,year)
    when 4
      exit
    else
      puts "Dado inválido!"
      end
    end
  end
end
